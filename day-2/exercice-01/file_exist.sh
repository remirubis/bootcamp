#!/bin/bash

if [ -f "$1" ]; then
    echo "${PWD}/$1 exists."
else 
    echo "${PWD}/$1 does not exist"
fi
