#!/bin/bash

my_swap () {
    echo "A = $2 and B = $1"
}

if [ -z "$1" ]; then
    echo "No arguments"
else 
    my_swap $1 $2
fi

