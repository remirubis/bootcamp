#!/bin/bash

var=$1

for (( i=0; i<${#var}; i++ )); do 
    car=${var:$i:1}
    if [[ $car == [A-Z] ]];
    then
        var=${var/[$car]/${car,,}}
    else
        var=${var/[$car]/${car^^}}
    fi
done

echo $var
