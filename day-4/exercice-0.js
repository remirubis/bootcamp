export const my_factorial = (nbr) => {
  if (typeof nbr !== "number") {
    return;
  }
  if (nbr === 0) {
    return 1;
  }

  return nbr * my_factorial(nbr - 1);
};
