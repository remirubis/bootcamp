import { promises as fs } from 'fs';

export const mySuperHerosTree = (heros, path = "./") => {
  if (heros.length === 0) {
    return;
  }

  Object.keys(heros).forEach(async (key) => {
    if (typeof heros[key] === "object") {
      await fs.mkdir(path + key, { recursive: true }, () => {});
      mySuperHerosTree(heros[key], path + key);
    } else {
      await fs.writeFile(path + key, heros[key], { recursive: true }, () => {});
    }
  });
  
  return;
};
