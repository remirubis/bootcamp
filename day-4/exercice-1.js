export class My_Wallet {
  constructor (sum) {
    this.sum = typeof sum !== "number" ? 0 : sum;
  }

  get () {
    return this.sum + "€";
  }

  add (nbr) {
    if (typeof nbr !== "number") {
      return new Error("not is a number");
    }
    this.sum += (nbr < 0) ? 0 : nbr;

    return `you have adding ${nbr}€`;
  }

  remove (nbr) {
    if (typeof nbr !== "number") {
      return new Error("not is a number");
    }
    if (nbr < 0) {
      return;
    }
    if(this.sum >= nbr) {
      nbr = this.sum;
      this.sum = 0;
    } else {
      this.sum -= nbr;
    }
    
    return `you have remove ${nbr}€`;
  }
}
