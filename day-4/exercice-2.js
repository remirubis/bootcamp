export const my_notation_classe = (arr) => {
  if (!Array.isArray(arr) || arr.length === 0) {
    return;
  }
  let result = arr.reduce((a, b) => a + b) / arr.length;

  return result;
};
