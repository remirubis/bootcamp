export const my_display_combv2 = () => {
  let list = [];
  let nb1 = 0;
  let nb2 = 1;
  let nb3 = 2;
  let round = 0;
  
  while (nb1 <= 9) {
    while (nb2 <= 9) {
      while (nb3 <= 9) {
        list[round] = `${nb1} ${nb2} ${nb3}`;
        nb3++;
        round++;
      }
      nb3 = nb2 + 2;
      nb2++;
    }
    nb2 = nb1 + 1;
    nb1++;
  }
  
  return list;
};
