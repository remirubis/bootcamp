export const my_display_unicode = (arr) => {
  let phrase = "";
  
  arr.forEach(char => {
    let format = `\\u${char.substring(2)}`;
    let character = decodeURIComponent(JSON.parse('"' + format.replace(/\"/g,'\\"') + '"'));
    if (/[A-zÀ-ú0-9 ]/.test(character)) {
      phrase += character;
    }
  });

  return phrase;
};
