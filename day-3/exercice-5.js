export const my_display_comb = () => {
  let array = [];
  let i = 0;
  let j = 0;
  let round = 0;

  while (i <= 99) {
    while (j <= 99) {
      array[round] = `${i} ${j}`;
      j++;
      round++;
    }
    j = 0;
    i++;
    round++;
  }
  
  return array;
};
