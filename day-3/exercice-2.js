export const my_size_alpha = (str) => {
  let nb = 0;

  if (typeof str !== "string") {
    return 0;
  } else {
    while (str[nb]) {
      nb++;
    }
  }
  
  return nb;
};
