export const my_sum = (a, b) => {
  if (!a && !b) {
    return 0;
  }
  
  return a + b;
};
